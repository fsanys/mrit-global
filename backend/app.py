import uuid
from bson.objectid import ObjectId
from flask import Flask, jsonify, request, Response
from flask_cors import CORS
from pymongo import MongoClient
from os import environ
import sys
import json
import logging

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# get Mongo IP
MONGO_IP = environ.get('ENV_MONGO_IP')

if MONGO_IP is None:
    logging.critical("IP of Mongo Database is not defined")
    sys.exit()
else:
    logging.warning("Mongo Databse IP : " + MONGO_IP)



MONGO_CONNECTION = 'mongodb://' + MONGO_IP + '/movie-bag'

client = MongoClient(MONGO_CONNECTION)
db = client['movies']


# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})
#CORS(app)


# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

@app.route('/movies', methods=['POST', 'GET'])
def data():
    
    # POST a data to database
    if request.method == 'POST':
        body = request.json
        body_title = body['title']
        body_rate = body['rate'] 
        # db.users.insert_one({
        db['movies'].insert_one({
            "title": body_title,
            "rate": body_rate
        })
        return jsonify({
            'status': 'Data is posted to MongoDB!',
            "title": body_title,
            "rate": body_rate
        })
    
    # GET all data from database
    if request.method == 'GET':
        allData = db['movies'].find()
        dataJson = []
        for data in allData:
            id = data['_id']
            body_title = data['title']
            logging.warning("Title : " + body_title)
            body_rate = data['rate']
            dataDict = {
                'id': str(id),
                'title': body_title,
                'rate': body_rate
            }
            dataJson.append(dataDict)
        print(dataJson)
        return jsonify(dataJson)

@app.route('/movies/<string:id>', methods=['GET', 'DELETE', 'PUT'])
def onedata(id):

    # GET a specific data by id
    if request.method == 'GET':
        data = db['movies'].find_one({'_id': ObjectId(id)})
        id = data['_id']
        body_title = data['title']
        body_rate = data['rate']
        dataDict = {
            'id': str(id),
            'title': body_title,
            'rate': body_rate
        }
        print(dataDict)
        return jsonify(dataDict)
        
    # DELETE a data
    if request.method == 'DELETE':
        db['movies'].delete_many({'_id': ObjectId(id)})
        print('\n # Deletion successful # \n')
        return jsonify({'status': 'Data id: ' + id + ' is deleted!'})

    # UPDATE a data by id
    if request.method == 'PUT':
        body = request.json
        body_title = body['title']
        body_rate = body['rate']

        db['movies'].update_one(
            {'_id': ObjectId(id)},
            {
                "$set": {
                    "title":body_title,
                    "rate":body_rate
                }
            }
        )

        print('\n # Update successful # \n')
        return jsonify({'status': 'Data id: ' + id + ' is updated!'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, threaded=True)

