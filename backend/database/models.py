from .db import db

class Movie(db.Document):
    title = db.StringField(required=True, unique=True)
    rate = db.IntField(required=True)
